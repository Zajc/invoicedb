import config.ConfigurationService;

import static config.Key.PROPERTIES_INDEX;

public class Demo {
    public static void main(String[] args) {
        ConfigurationService cfg = new ConfigurationService(args[PROPERTIES_INDEX]);
        cfg.load();
    }
}
