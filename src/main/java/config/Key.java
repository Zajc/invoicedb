package config;

public interface Key {
    String DB_NAME="db.name";
    String DBSM="dbms";
    String USER_NAME="userName";
    String SERVER_NAME="serverName";
    String PORT_NUMBER="portNumber";
    String PASSWORD="password";
    int PROPERTIES_INDEX = 0;
}
