package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

import static config.Key.*;
import static config.Key.DB_NAME;

public class ConfigurationService {
    private Properties properties = new Properties();
    private String path;
    private InputStream input = null;
    private Connection conn;

    public ConfigurationService(String path) {
        this.path = path;
    }

    public void load() {
        try {
            input = new FileInputStream(path);

            properties.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String get(String key) {
        return properties.getProperty(key);
    }

    public Connection getConnection() throws SQLException {
        Properties connectionProps = new Properties();
        connectionProps.put("User", get(USER_NAME));
        connectionProps.put("password", get(PASSWORD));
        conn = DriverManager.getConnection(
                "jdbc:" + get(DBSM) + "://" +
                        get(SERVER_NAME) +
                        ":" + get(PORT_NUMBER) + "/" +
                        get(DB_NAME),
                connectionProps);
        return conn;
    }

    void testConnection(Connection con) throws SQLException {
        Statement stmt = null;
        String query = "SELECT USER();";
        try {
            stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery(query);

            while (resultSet.next()) {
                String user = resultSet.getString(1);
                System.out.println(user);
            }
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
    }
}
