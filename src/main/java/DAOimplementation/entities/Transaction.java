package DAOimplementation.entities;

import java.util.Date;

public class Transaction implements Entiti {
    private int id;
    private String clientName;
    private String invoiceNumber;
    private Date date;

    public Transaction(int id, String clientName, String invoiceNumber, Date date) {
        this.id = id;
        this.clientName = clientName;
        this.invoiceNumber = invoiceNumber;
        this.date = date;
    }

    public Transaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", clientName='" + clientName + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", date=" + date +
                '}';
    }
}
