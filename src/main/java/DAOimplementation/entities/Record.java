package DAOimplementation.entities;

public class Record implements Entiti {
    private int id;
    private String name;
    private int quantity;
    private String unit;
    private double netPrice;
    private int vat;

    public Record(int id, String name, int quantity, String unit, double netPrice, int vat) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.netPrice = netPrice;
        this.vat = vat;
    }

    public Record() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public int getVat() {
        return vat;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", unit='" + unit + '\'' +
                ", netPrice=" + netPrice +
                ", vat=" + vat +
                '}';
    }
}
