package DAOimplementation.SQLgenerators;

import DAOimplementation.entities.Record;

public class RecoordsSQLGenerator implements SQLGenerator<Record> {
    private String tableName;

    public RecoordsSQLGenerator(String tableName) {
        this.tableName = tableName;
    }

    public String insert(Record toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(tableName).append (" (id, name, quantity, unit, netPrice, vat) VALUES (")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getName()).append("', ")
                .append(toInsert.getQuantity()).append(", '")
                .append(toInsert.getUnit()).append("', ")
                .append(toInsert.getNetPrice()).append(", ")
                .append(toInsert.getVat()).append(");");
        return sb.toString();
    }

    public String selectALL() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ").append(tableName);
        return sb.toString();
    }

    public String update(Record toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(tableName).append(" SET name= '").append(toUpdate.getName())
                .append("', quantity= ").append(toUpdate.getQuantity())
                .append(", unit= '").append(toUpdate.getUnit())
                .append("', netPrice= ").append(toUpdate.getNetPrice())
                .append(", vat= ").append(toUpdate.getVat())
                .append(" WHERE (id=").append(toUpdate.getId()).append(");");
        return sb.toString();
    }

    public String delete(Record toDelete) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ").append(tableName)
                .append(" WHERE id= ").append(toDelete.getId()).append(";");
        return sb.toString();
    }
}
