package DAOimplementation.SQLgenerators;

import DAOimplementation.entities.Transaction;

public class TransactionSQLGeneraror implements SQLGenerator<Transaction> {

    public String insert(Transaction toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO transactions (id, client_name, invoice_numer, date) VALUES (")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getClientName()).append("', '")
                .append(toInsert.getInvoiceNumber()).append("', '")
                .append(toInsert.getDate()).append("');");
        return sb.toString();
    }

    public String selectALL() {
        return "SELECT * FROM transactions";
    }

    public String update(Transaction toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE transactions SET client_name= '").append(toUpdate.getClientName())
                .append("', invoice_numer= '").append(toUpdate.getInvoiceNumber())
                .append("', date= '").append(toUpdate.getDate())
                .append("' WHERE (id=").append(toUpdate.getId()).append(");");
        return sb.toString();
    }

    public String delete(Transaction toDelete) {
        return "DELETE FROM transactions WHERE id=" + toDelete.getId() + ";";
    }
}
