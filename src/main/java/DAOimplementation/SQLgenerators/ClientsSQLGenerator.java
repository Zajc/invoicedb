package DAOimplementation.SQLgenerators;

import DAOimplementation.entities.Client;

public class ClientsSQLGenerator implements SQLGenerator<Client> {

    public String insert(Client toInsert) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO clients (id, name, address, city, zip, nip) VALUES (")
                .append(toInsert.getId())
                .append(", '").append(toInsert.getName()).append("', '")
                .append(toInsert.getAddress()).append("', '")
                .append(toInsert.getCity()).append("', '")
                .append(toInsert.getZipCode()).append("', '")
                .append(toInsert.getNip()).append("');");
        return sb.toString();
    }

    public String selectALL() {
        return "SELECT * FROM clients";

    }

    public String update(Client toUpdate) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE clients SET name= '").append(toUpdate.getName())
                .append("', address= '").append(toUpdate.getAddress())
                .append("', city= '").append(toUpdate.getCity())
                .append("', zip= '").append(toUpdate.getZipCode())
                .append("', nip= '").append(toUpdate.getNip())
                .append("' WHERE (id=").append(toUpdate.getId()).append(");");
        return sb.toString();
    }

    public String delete(Client toDelete) {
        return "DELETE FROM clients WHERE id=" + toDelete.getId() + ";";
    }
}
