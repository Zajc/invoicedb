package DAOimplementation.DAOs;

import DAOimplementation.SQLgenerators.ClientsSQLGenerator;
import DAOimplementation.entities.Client;
import config.ConfigurationService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientsDAO extends BaseDAO<Client> {
    private ClientsSQLGenerator sqlGenerator = new ClientsSQLGenerator();

    public ClientsDAO(ConfigurationService cfg) {
        super(cfg);
    }

    @Override
    public void insert(Client toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Client> select() {
        sql = sqlGenerator.selectALL();
        return executeSelect();
    }

    @Override
    public void update(Client toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Client toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Client> parse(ResultSet rs) {
        ArrayList<Client> resolt = new ArrayList<Client>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String address = rs.getString("address");
                String city = rs.getString("city");
                String zipCode = rs.getString("zip");
                String nip = rs.getString("nip");

                resolt.add(new Client(id, name, address, city, zipCode, nip));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resolt;
    }
}
