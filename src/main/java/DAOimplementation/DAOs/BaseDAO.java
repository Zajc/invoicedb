package DAOimplementation.DAOs;

import DAOimplementation.entities.Entiti;
import config.ConfigurationService;

import java.sql.*;
import java.util.List;


public abstract class BaseDAO<T extends Entiti> {
    private ConfigurationService cfg;
    protected String sql;
    private Connection conn;

    public BaseDAO(ConfigurationService cfg) {
        this.cfg = cfg;
    }

    public abstract void insert(T toInsert);

    public abstract List<T> select();

    public abstract void update(T toUpdate);

    public abstract void delete(T toDelete);


    public void execute() {
        try {
            conn = cfg.getConnection();
            Statement stet = conn.createStatement();
            stet.execute(sql);

            stet.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected abstract List<T> parse(ResultSet rs);

    protected List<T> executeSelect() {
        try {
            conn = cfg.getConnection();
            Statement stet = conn.createStatement();
            ResultSet rs = stet.executeQuery(sql);
            List<T> result = parse(rs);
            rs.close();
            stet.close();
            conn.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
