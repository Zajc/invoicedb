package DAOimplementation.DAOs;

import DAOimplementation.SQLgenerators.TransactionSQLGeneraror;
import DAOimplementation.entities.Transaction;
import config.ConfigurationService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionsDAO extends BaseDAO<Transaction> {
    private TransactionSQLGeneraror sqlGenerator = new TransactionSQLGeneraror();

    public TransactionsDAO(ConfigurationService cfg) {
        super(cfg);
    }

    @Override
    public void insert(Transaction toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }

    @Override
    public List<Transaction> select() {
        sql = sqlGenerator.selectALL();
        return executeSelect();
    }

    @Override
    public void update(Transaction toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }

    @Override
    public void delete(Transaction toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }

    @Override
    protected List<Transaction> parse(ResultSet rs) {
        ArrayList<Transaction> resolt = new ArrayList<Transaction>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String clientName = rs.getString("client_name");
                String invoiceNumber = rs.getString("invoice_number");
                Date date = rs.getDate("date");

                resolt.add(new Transaction(id, clientName, invoiceNumber, date));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resolt;
    }
}
