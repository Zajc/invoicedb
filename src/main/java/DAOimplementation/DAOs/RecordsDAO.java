package DAOimplementation.DAOs;

import DAOimplementation.SQLgenerators.RecoordsSQLGenerator;
import DAOimplementation.entities.Record;
import config.ConfigurationService;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RecordsDAO extends BaseDAO<Record> {
    private String invoiceNumber;
    private RecoordsSQLGenerator sqlGenerator;
    public RecordsDAO(ConfigurationService cfg) {
        super(cfg);
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
        sqlGenerator=new RecoordsSQLGenerator(invoiceNumber);
    }
    @Override
    public void insert(Record toInsert) {
        sql = sqlGenerator.insert(toInsert);
        execute();
    }
    @Override
    public List<Record> select() {
        sql = sqlGenerator.selectALL();
        return executeSelect();
    }
    @Override
    public void update(Record toUpdate) {
        sql = sqlGenerator.update(toUpdate);
        execute();
    }
    @Override
    public void delete(Record toDelete) {
        sql = sqlGenerator.delete(toDelete);
        execute();
    }
    @Override
    protected List<Record> parse(ResultSet rs) {
        ArrayList<Record> resolt = new ArrayList<Record>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int quantity = rs.getInt("quantity");
                String unit = rs.getString("unit");
                double zipCode = rs.getDouble("net_price");
                int vat = rs.getInt("vat");

                resolt.add(new Record(id, name, quantity, unit, zipCode, vat));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resolt;
    }
}
